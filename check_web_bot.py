#!/usr/bin/env python3
import telepot
import requests
import hashlib
import time

url_list = {\
'https://docs.google.com/document/u/1/export?format=txt&id=1JsGPaquauyaZotjeuFRp0EsWSuUlYOCL-3nkP52Fawk&token=AC4w5ViMrr-WkSQSdOiju0MdXSHTBeD-NQ%3A1525377582611&includes_info_params=true': '雲端作業',\
'http://www.cc.ntut.edu.tw/~htwu/courses/SP2018PB/SP2018PB.htm': '機率網頁',\
'http://www.cc.ntut.edu.tw/~cliu/courses/ad/ad.htm': 'Android作業'}

# 讀取 bot_token.txt 裡的 telegram bot token
with open('bot_token.txt', 'r') as f:
    bot = telepot.Bot(f.read().strip())

# 計算資料 MD5 雜湊值
def md5(data):
    m = hashlib.md5()
    m.update(data)
    return m.hexdigest()

# 檢查Android課程成績更新
def checkAndroidScore(password):
    data = {'passwd': password, 'showGrade': 'Show Grade'}
    response = requests.post("http://www.cc.ntut.edu.tw/~cliu/courses/ad/grade/showGrade.php", data=data)

    # 產稱新的md5
    new_hash = md5(response.content)
    # 讀取舊的md5
    old_hash = ""    
    try:
        with open('android_grade.txt', 'r') as f:
            old_hash = f.read()
    except FileNotFoundError:
        open('android_grade.txt', 'w').close()
        
    # 比較新舊 md5 來判斷網頁是否變更，如果有變更則發訊息
    if old_hash != new_hash and old_hash != '':
        bot.sendMessage(259067101, 'Android成績更新囉！')

    # 將新的 md5 寫進檔案
    with open('android_grade.txt', 'w') as f:
        f.write(new_hash)


def checkWebList():
    for url, msg in url_list.items():
        response = requests.get(url)

        # 產稱新的 md5
        new_hash = md5(response.content)
        # 讀取舊的 md5
        old_hash = ""
        try:
            with open('{}.txt'.format(msg), 'r') as f:
                old_hash = f.read()
        except FileNotFoundError:
            open('{}.txt'.format(msg), 'w').close()

        # 比較新舊 md5 來判斷網頁是否變更，如果有變更則發訊息
        if old_hash != new_hash and old_hash != '':
            bot.sendMessage(259067101, '{}更新囉！'.format(msg))

        # 將新的 md5 寫進檔案
        with open('{}.txt'.format(msg), 'w') as f:
            f.write(new_hash)

if __name__ == '__main__':
    while True:
        checkAndroidScore('105810037')
        checkWebList()
        time.sleep(600)
