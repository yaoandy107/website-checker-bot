# Website Checker Bot
A bot used to check if the web have been changed.
## Usage
1. Add the bot token inside the `bot_token.txt`
2. Run the install.sh to setup the required python package
3. Run `check_web_bot.py` with nohup or tmux
